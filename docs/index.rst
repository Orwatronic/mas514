.. MAS507 documentation master file, created by
   sphinx-quickstart on Tue Aug 25 21:04:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: ./figs/uia.png
   :width: 600px
   :align: center
   :alt: alternate text

|

.. raw:: html

   <h2 style="text-align: center">University of Agder,2021</h2>
   <p style="text-align: center">Department of Engineering Sciences<br>Grimstad, Norway</p>
   
|

.. image:: ./figs/jetbot.jpg
   :width: 12cm
   :align: center

|
|

Course MAS-514 - Navigation Stack and SLAM
--------------
   - Hussam Haij
   - Mohammed Alsaadi
   - Orwah Zubaidi





SUPERVISOR
 - Daniel Hagen, PhD







.. toctree::
   :maxdepth: 2
   :caption: Contents:

   src/Navigation_Stack
   src/Navigation_Stack_Repo
   src/PC_and_jetbot_Connection






    


.. Indices and tables
.. ==================



.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
