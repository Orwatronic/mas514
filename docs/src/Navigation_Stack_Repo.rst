Navigation stack repo
======================

 Our catkin work space can be cloned and used instead of set up the navigation stack, to do so, first open new terminal and and clone our catkin_ws
    
    - :code:`$ git clone https://github.com/Orwatronic/mas_514.git`

 Then, navigate to our catkin_ws and build the packages 
    
    - :code:`$ cd ~/catkin_ws`
    - :code:`$ catkin_make`

 If everything works correctly, we can start mapping. In order to build a map there are two ways to do it. Hector SLAM mapping and Gmapping -SLAM mapping.

Hector SLAM (Mapping using only laser scan data)
---------------------------------------------------

 To start mapping launch the different files each in separate terminal. First, we start ros master
    
    - :code:`$ roscore`

 then, to move the jetbot around we must launch the teleoperation node, 
    
    - :code:`$ export TURTLEBOT3_MODEL=burger`
    - :code:`$ roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch`

 Also, we need turn on jetbot kinematics
    
    - :code:`$ roslaunch jetbot_nav jetbot.launch`

 And launch the laser node  to publish the need laser scan data
    
    - :code:`$ roslaunch jetbot_nav laser.launch`

 Finaly, launch hector slam mapping
    
    - :code:`$ roslaunch hector_slam_launch tutorial.launch`

 Move the jetbot using the keyboard to draw the map

save the map
--------------

 Install map server
    
    - :code:`$ sudo apt-get install ros-melodic-map-server`

 Navigate to maps directory
    
    - :code:`$ cd ~/catkin_ws/src/mas514/maps`

 Save the map in pgm and yaml format
    
    - :code:`$ rosrun map_server map_saver -f  map`

Gmapping SLAM (Mapping using  laser scan and odometry data)
--------------------------------------------------------------

 To start mapping, run the following codes

 In new terminal
   
    - :code:`$ roslaunch jetbot_nav jetbot.launch`

 In new terminal 
    
    - :code:`$ roslaunch jetbot_nav laser.launch`

 In new terminal 
    
    - :code:`$ export TURTLEBOT3_MODEL=burger`
    - :code:`$ roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch`

 In new terminal 
    
    - :code:`$ export TURTLEBOT3_MODEL=burger`
    - :code:`$ roslaunch turtlebot3_slam turtlebot3_gmapping.launch`

 Move the jetbot around to draw the map slowly, and then save the map if the result is good enough. 
 After we done this, we will have the needed map to be used in the navigation stack.

Save The Map
----------------

 Install map server
    
    - :code:`$ sudo apt-get install ros-melodic-map-server`

 Navigate to maps directory
    
    - :code:`$ cd ~/catkin_ws/src/mas514/maps`

  Save the map in pgm and yaml format
    
    - :code:`$ rosrun map_server map_saver -f  map`

Navigation Stack
------------------

Launch these three files in separate terminal to start navigation
    
    - :code:`$ roslaunch jetbot_nav jetbot.launch`
    - :code:`$ roslaunch jetbot_nav laser.launch`
    - :code:`$ roslaunch jetbot_nav move_base.launch`

 Now, this will open up Rvis as bellow, add the topics as in the picture and click on the green arrow, then, click anywhere in the map to set the initial position of the jetbot. Then, click on the goal button, then lick anywhere in the map to set the goal destination ! now watch your jetbot navigate.

 .. figure:: ../figs/Picture15.png
    :figclass: align-center