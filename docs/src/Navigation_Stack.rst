Navigation Stack Set Up
==========================

 This inspired by :

 - `ROS <http://wiki.ros.org/navigation/Tutorials/RobotSetup>`_

 - `Daniel Hagen <https://hagenmek.gitlab.io/mas514/src/start.html>`_

 - `Automatic Addison <https://automaticaddison.com/how-to-set-up-the-ros-navigation-stack-on-a-robot/>`_

 As start we assumed that you have mas514 package in your catkin work space if not please refer to the following `link <https://hagenmek.gitlab.io/mas514/src/start.html>`_

 The ROS Navigation Stack is a collection of software packages that you can use to help your robot move from a **starting location to a goal location** safely.

 In order to build the Navigation stack you have to build and set up several packages, as in the figure bellow, The white components are required components that are already implemented in Ros, the gray components are optional components that are already implemented, and the blue components must be created for each robot platform. 

 .. figure:: ../figs/Navigation_Stack.JPG
    :figclass: align-center

    Navigation Stack

 Now, let us build the navigation stack for Jetbot step by step. First, open your Jetbot terminal and navigate to your workspace create the first package for navigation, for me it’s called jetbot_nav, then build it 

    - :code:`$ cd ~/catkin_ws/src`
    - :code:`$ catkin_create_pkg jetbot_nav rospy roscpp std_msgs tf tf2_ros geometry_msgs sensor_msgs move_base`
    - :code:`$ cd ~/catkin_ws/`
    - :code:`$ catkin_make --only-pkg-with-deps jetbot_nav`

 Now we’re going to put together our launch file. Open a new terminal window, and move to your jetbot_nav package.
    
    - :code:`$ roscd jetbot_nav`
    - :code:`$ mkdir launch`
    - :code:`$ cd launch`

 Create your launch file. Mine called jetbot.launch
    
    - :code:`$ gedit jetbot.launch`

 Add the following code:

 .. code-block::

    <launch>
    </launch>

 Save the file, and close it.

Transform configuration
------------------------

 The ros navigation stack require that we publish and set the relationship between the different coordinates frames of the robot using tf package. 

 Open a terminal window and type:
    
    - :code:`$ roscd jetbot_nav`
    - :code:`$ cd launch`

 Open your launch file.
    
    - :code:`$ gedit jetbot.launch`

 Add the following lines to configure the transformation frams between the different parts of the robot.

 .. code-block::

    <!-- Transformation Configuration ... Setting Up the Relationships Between Coordinate Frames --> 

    <node pkg="tf" type="static_transform_publisher" name="base_link_to_camera_link" args="0.06 0 0.08 0 0 0 base_link camera_link 30" />

    <node pkg="tf" type="static_transform_publisher" name=" camera_link_to_laser" args="0 0 0 0 0 0 camera_link laser 30" />

    <!-- map to odom will be provided by the AMCL -->

    <node pkg="tf" type="static_transform_publisher" name="map_to_odom" args="0 0 0 0 0 0 map odom 30" />


 Save the file and close it.

Sensor Information (Laser Scan Information)
--------------------------------------------

 The ROS Navigation Stack uses sensor information to help the robot avoid obstacles in the environment. It assumes that the sensor publishes either **sensor_msgs/LaserScan** ok or **sensor_msgs/PointCloud** messages over ROS.

 This can be done by following this guide `Daniel Hagen <https://hagenmek.gitlab.io/mas514/src/guides.html#install-realsense2-ros>`_ . Following this guide will create two packages one called realsense-ros and pointcloud to laser scan. Which will provided us with the needed nodes and topics for sensor information. However, we will need to do some changes to optimize the functionality of the sensor.

 Open file :code:`catkin_wss/src/realsense-ros/realsense2_camera/launch/rs_camera.launch` , and scroll to :code:`line 46` and set enable pointcloud to **true** , and :code:`line 47` replace :code:`“RS”_STREAM_COLOR` to :code:`“RS”_STREAM_ANY”`, save the file and close it.

 Open file :code:`catkin_wss/src/pointcloud_to_laserscan/launch/sample_node.launch`, and scroll to :code:`line 26` and :code:`change concurrency_level : 1` ,and to :code:`line 6` and change :code:`“camera/scan”` to :code:`“scan”` save the file and close it.

 Now open terminal and navigate to the launch folder in jetbot_nav  and create a second launch file called laser.launch, as follow:
    
    - :code:`$ roscd jetbot_nav`
    - :code:`$ cd launch` 
    - :code:`$ gedit laser.launch`

 Copy both lunch files :code:`rs_camera.launch in catkin_wss/src/realsense-ros/realsense2_camera/launch/rs_camera.launch`,  and :code:`sample_node.launch` in  :code:`catkin_wss/src/pointcloud_to_laserscan/launch/sample_node.launch`, and paste them in :code:`laser.launch` file, save and close. To check if it works you can, excute this code in the terminl.
    
    - :code:`$ roslaunch jetbot_nav laser.launch`
    - :code:`$ rostopic list`

 You should find a topic called :code:`“/scan”`. Now, sensor information ready to use and we have the need topic which called :code:`“scan”`. 

 .. figure:: ../figs/Picture2.png
    :figclass: align-center
    
Odometry Information (odometry source) 
--------------------------------------

 The navigation stack requires that odometry information be published using **tf** and the **nav_msgs/Odometry** message. To do so first, the odometry node should sub-scribe to the encoder node that built in Arduino using rosserial, to get the right and left an-gle for the wheels. This can be done by following Automatic Addison `Daniel Hagen <https://hagenmek.gitlab.io/mas514/src/guides.html#how-to-publish-wheel-encoder-data-using-ros-and-arduino>`_ . By doing this guide you will be able to publish two topics :code:`/angle_right_wheel` and :code:`/angle_left_wheel`, which is need for to publish the odometry information. However, to optimize the data, we have to make some changes. First we will add two pass filter to the data of the angle, under **// Function for reading right wheel angle and number of turns**

 In encoder_ros.ino file replace 
    
    - :code:`angleRight.data = angle_right;` 

 With the following code:

    - :code:`angle_right1 = fGain*angle_right1 + (1-fGain)*angle_right;`
    - :code:`angle_right2 = fGain*angle_right2 + (1-fGain)*angle_right1;`
    - :code:`angleRight.data = angle_right2;`

 And under **// Function for reading left wheel angle and number of turns**

 Replace 

    - :code:`angleLeft.data = angle_Left;`

 With the following code,

    - :code:`angle_left1 = fGain*angle_left1 + (1-fGain)*angle_left;`
    - :code:`angle_left2 = fGain*angle_left2 + (1-fGain)*angle_left;`
    - :code:`angleLEFT.data = angle_left2;`

 And add this under variable at the beginning of the code
   
    - :code:`static volatile int angle_left, angle_left1, angle_left2;`
    - :code:`static volatile int angle_right, angle_right1, angle_right2;`
 And this under parameters

    - :code:`static const float fGain = 0.98;`

 And change the buad rate to 57600 and time interval to 30 ms, save, verify and upload to the Arduino.

 To verify if everything has been done correctly, open jetbot terminal and run the following 
    
    - :code:`$ roscore`

 Open a new terminal and run 

    - :code:`$ rosrun rosserial_arduino serial_node.py port:=/dev/ttyUSB0 _buad:=57600`

 Open a new terminal and run 

    - :code:`$ rostopic list`

 You should get **/angle_left_wheel and /angle_right_wheel**

 .. figure:: ../figs/Picture3.png
    :figclass: align-center

 Now, since we have the needed data from the encoders, we need to build the odometry publisher node, to do this, follow `Daniel Hagen <https://hagenmek.gitlab.io/mas514/src/guides>`_  guide. (note: place the odometry.py file in the guide inside “catkin_ws/src/mas514/src”)  , the odometry data will have some jumps caused by the wheel angle data when it became more than 32,768 (int 16 ), and in order to solve this problem we have to add some correction inside the Odometry.py file, open the file and scroll down under 
    
 - Convert Encoder signal in deg to rad

 .. code-block:: 

    thetaL = -angle_left*angle2rad
    thetaR = angle_right*angle2rad

 And add these lines :

 .. code-block:: 

    If abs(thetaL – thetaL_old) > 3000 :
        thetaL_old = thetaL
        continue
    If abs(thetaR – thetaR_old) > 3000 :
        thetaR_old = thetaR
		continue

 Now we need to add this to nodes to our launch file jetbot.launch

    - :code:`$ roscd jetbot_nav` 
    - :code:`$ cd launch` 
    - :code:`$ gedit jetbot.launch`

 And add the following:

 .. code-block::

    <!—Wheel odometry publisher -->
    <node name="Odometry" pkg="mas514" type="Odometry.py" output="screen"/>
    <node name="serial_node" pkg="rosserial_python" type="serial_node.py" output="screen">
    <param name="port" value="/dev/ttyUSB0"/>
    <param name="baud" value="57600"/>

    Save the file and close it.


 Now in order to verfiy everything is ok, launch jetbot.launch file

    - :code:`$ roslaunch jetbot_nav jetbot.launch`
    - :code:`$ rostopic list`

    You should have **/odom** topic, now the odometry information is ready.

Base controller
----------------

 The ROS Navigation Stack requires a node that subscribes to the “cmd_vel” (i.e. velocity command) topic that takes velocities and converts them into motor commands. Since, this guide assumed that you already cloned mas514 package (if not please refer to `Daniel Hagen <https://hagenmek.gitlab.io/mas514/src/start.html>`_ , then you have InverseKinematics.py file inside “mas514/src”, which we will edit it so it subscribe to the “/cmd_vel” topic published by the move_bose node (built in package in Ros publishes “/cmd_vel” topic, refer to the figure at the beginning of this guide ), and to change this file copy this code  and replace it, this will publish servo set point that needed in jetbot controller to give the motors command.   

 Now, we have to add this nodes to our launch file :code:`“jetbot.launch”`

 Open new terminal and run this

    - :code:`$ roscd jetbot_nav`
    - :code:`$ cd launch`
    - :code:`$ gedit jetbot.launch`

 Add these two lines under the Odometry.py node

 .. code-block::

    <node name="InverseKinematics" pkg="mas514" type="InverseKinematics.py" output="screen"/>
    <node name="JetbotController " pkg="mas514" type="JetbotController.py" output="screen"/>  

 Save and close it. So far we can move the robot if we are able to send velocity commands over the “/cmd_vel” topic , and publish  the odometry information over “/odom” topic, and publish the laser scan data over the “/scan” topic. To check everything, we can use the keyboard to send data over /cmd_vel and check.

Teleoperation using keyboard
------------------------------

 Using turtlebot3 packages we can control our robot and move around using the keyboard, to do so, go to this guide and do 1a (set up), after you have done that. Open new terminal and run the following 
   
    - :code:`$export TURTLEBOT3_MODEL=burger`
    - :code:`$roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch`

 If everything is works correctly, open a new terminal and write
    
    - :code:`$ rostopic list`

 You should see **/cmd_vel** topic. To move the robot you can run this on another terminal
    
    - :code:`$ roslaunch jetbot_nav jetbot.launch`

 **And Run**
   
    - :code:`$ rostopic list`

 To check if the following topics are running

 .. code-block::

    /cmd_vel
    /angle_left_wheel
    /angle_rightwhell 
    /odom
    /servosetpoints


 If everthings is ok, click inside the **roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch**  terminal and start moving the robot using your keyboard (W,X,A,D and S).  now since we can control the robot and move it around and we have the odometry and laser scan information, we can start mapping.

SLAM Mapping
--------------

 There are sevral methods to build a map, we will go through two methods Hector-SLAM and Gmapping_SLAM. Hector-SLAM is an algorithm that uses laser scan data to create a map. The advantage of Hector-SLAM over other SLAM techniques is that it only requires laser scan data to do its job. It doesn’t need odometry data., where Gmapping needs both laser scan and odometry data to build a map. 

Hector SLAM (Mapping using only laser scan data)
-------------------------------------------------

 Step1: install Qt4

    - :code:`$ sudo apt-get install qt4-qmake qt4-dev-tools`

 Step 2: Download Hector SLAM package

    - :code:`$ cd ~/catkin_ws/src`
    - :code:`$ git clone https://github.com/tu-darmstadt-ros-pkg/hector_slam.git`

 Step 3: Set coordinate frames

    - :code:`$ sudo gedit ~/catkin_ws/src/hector_slam/hector_mapping/$launch/mapping_default.launch`

  Search for these lines **(lines 5 and 6 in my code)**

    - :code:`<arg name="base_frame" default="base_footprint"/>`
    - :code:`<arg name="odom_frame" default="nav"/>`

  Change those lines to this:

    - :code:`<arg name="base_frame" default="base_link"/>`
    - :code:`<arg name="odom_frame" default="base_link"/>`

  Now go to the end of this file, and find these lines **(line 54 in my code)**.
   
    - :code:`<!--<node pkg="tf" type="static_transform_publisher" name="map_nav_broadcaster" args="0 0 0 0 0 0 map nav 100"/> -->`

  Change those lines to this (be sure to remove the comment tags (<!– and –>):
    
    - :code:`<node pkg="tf" type="static_transform_publisher" name="base_to_laser_broadcaster" args="0 0 0 0 0 0 base_link laser 100"/>`

  Save the file, and return to the terminal window.

  Type the following command.
   
    - :code:`$ cd ~/catkin_ws/src/hector_slam/hector_slam_launch/launch`

  Open the tutorial.launch file.
   
    - :code:`$ gedit tutorial.launch`

  Find this line (line 7 in my code).
   
    - :code:`<param name="/use_sim_time" value="true"/>`

  Change that line to:
    
    - :code:`<param name="/use_sim_time" value="false"/>`

  Save the file, and close it.

  Open a new terminal window, and type this command:
    
    - :code:`cd ~/catkin_ws/`

  Buld the packages
    
    - :code:`$ catkin_make`

  **If you see this error message…**

  “”Project ‘cv_bridge’ specifies ‘/usr/include/opencv’ as an include dir, which is not found. It does neither exist as an absolute directory nor in…””
    
    - :code:`$ cd /usr/include`
    - :code:`$ sudo ln -s opencv4/ opencv`

  Build the packages again.
   
    - :code:`$ cd ~/catkin_ws/`
    - :code:`$ catkin_make`

  Shutdown then turn the Jetson Nano on again.
    
    - :code:`$ sudo shutdown -h now`

 Step 4: Mapping using Hector SLAM **(only needs laser scan data)**

    - Now to start mapping launch the different files each in separate terminal. First, we start ros master
        
        - :code:`$ roscore`

   then, to move the jetbot around we must launch the teleoperation node, 
       
        - :code:`$ export TURTLEBOT3_MODEL=burger`
        - :code:`$ roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch`

    Also, we need turn on jetbot kinematics
       
        - :code:`$ roslaunch jetbot_nav jetbot.launch`

    And, publish some static transfomations needed in hector slam 
        
        - :code:`$ rosrun tf static_transform_publisher 0 0 0 0 0 0 map scanmatcher_frame 10`
        - :code:`$ rosrun tf static_transform_publisher 0.06 0 0.08 0 0 base_link camera_link 10`

    and launch the laser node  to publish the need laser scan data
        
        - :code:`$ roslaunch jetbot_nav laser.launch`

    Finaly, launch hector slam mapping
       
        - :code:`$ roslaunch hector_slam_launch tutorial.launch`

 Rviz will open and you should see something like this

 .. figure:: ../figs/Picture4.jpg
    :figclass: align-center

 Step 5: Save the Map

    1- Install map server
        
        - :code:`$ sudo apt-get install ros-melodic-map-server`

    2- Create maps folder to save your map in this directory
        
        - :code:`$ mkdir ~/catkin_ws/src/mas514/maps`

    3- launch mapping process in step 4
    
        When you are happy with the map that you see in rviz, you can save the map as my_map.yaml and my_map.pgm. Open a new terminal

    4- Navigate to maps directory
        
        - :code:`$ cd ~/catkin_ws/src/mas514/maps`

    5- Save the map in pgm and yaml format
        
        - :code:`$ rosrun map_server map_saver -f  map`

Gmapping-SLAM
----------------

 If you did the setup for the tortlebot3 you should have a package called  turtlebot3 inside catkin_ws/src/turtlebot3

 In the launch folder turtlebot3_slam/launch there is a launch file called turtlebot3_gmapping.launch. we will use this file for Gmapping-SLAM, how ever we need to change line 5 from “base_footprint” to “base_link”

 And now it’s ready to be used. 

 In order to start mapping, run the following codes

 In new terminal

        - :code:`$ roslaunch jetbot_nav jetbot.launch`

 In new terminal

        - :code:`$ roslaunch jetbot_nav laser.launch`

 In new terminal

        - :code:`$ export TURTLEBOT3_MODEL=burger`
        - :code:`$ roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch`

 In new terminal 

        - :code:`$ export TURTLEBOT3_MODEL=burger`
        - :code:`$ roslaunch turtlebot3_slam turtlebot3_gmapping.launch`

 Move the jetbot around to draw the map slowly, and then save the map if the result is good enough. Add the topics as in the figure and you should see something like this 

 .. figure:: ../figs/Picture5.png
    :figclass: align-center

 After we done this, we will have the needed map to be used in the navigation stack.

Rviz initial pose and goal
------------------------------

 Will show you how to use Ros and Rviz to set the initial pose (i.e. position and orientation) of a robot. Once this pose is set, we can then give the robot a series of goal locations that it can navigate to. 

 First, Let’s start by creating a new package. Open a new terminal window, and type the following command inside the **catkin_ws/src folder**:
    
    - :code:`$ cd ~/catkin_ws/src/`

 Create the package.
    
    - :code:`$ catkin_create_pkg localization rospy roscpp std_msgs tf tf2_ros geometry_msgs sensor_msgs nav_msgs`

 Now open a new terminal and move to your catkin workspace.
    
    - :code:`$ cd ~/catkin_ws/`

 Compile the package.
    
    - :code:`$ catkin_make --only-pkg-with-deps localization`

 Move inside the package.
    
    -:code:`$ cd ~/catkin_ws/src/localization /`

 Go inside the CMakeLists.txt file.
    
    - :code:`$ gedit CMakeLists.txt`

 Remove the hashtag on line 5 to make sure that C++11 support is enabled.

 Save and close the file.

 Open a terminal window in your Jetson Nano. Move to the src folder of the localization package.
    
    - :code:`$ cd ~/catkin_ws/src /localization /src`

 Open a new C++ file called rviz_click_to_2d.cpp.
    
    - :code:`$ gedit rviz_click_to_2d.cpp`

 Write the following code:

 .. literalinclude:: ../../src/Rviz.cpp
    :language: c++

 Save the file and close it.

 Now open a new terminal window, and type the following command:
    
    - :code:`$ cd ~/catkin_ws/src/jetson /localization /`
    - :code:`$ gedit CMakeLists.txt`

 Go to the bottom of the file. And add these lines

 .. code-block::

    INCLUDE_DIRECTORIES(/usr/local/lib)
    LINK_DIRECTORIES(/usr/local/lib)
    add_executable(rviz_click_to_2d src/rviz_click_to_2d.cpp)
    target_link_libraries(rviz_click_to_2d ${catkin_LIBRARIES})

 Save the file, and close it.

 Go to the root of the workspace.
    - :code:`cd ~/catkin_ws`

 Compile the code.
    
    - :code:`catkin_make --only-pkg-with-deps localization`

 To run the code, you would type the following commands:
    
    - :code:`roscore`

 In another terminal window, type:
    
    - :code:`rosrun localization rviz_click_to_2d`

 Then open another terminal, and launch RViz.
   
    - :code:`rviz`

 .. figure:: ../figs/Picture6.png
    :figclass: align-center

 In another terminal type:
    
    - :code:`$ rostopic echo /initial_2d`

 In another terminal type:
    
    - :code:`$ rostopic echo /goal_2d`

 Then on Rviz, you can click the 2D Pose Estimate button to set the pose. You click on the button and then click on somewhere in the environment to set the pose.

 Then click the 2D Nav Goal button to set the goal destination. The output will print out to the terminal windows.

 Here is what you should see in the terminal windows:

 .. figure:: ../figs/Picture7.png
    :figclass: align-center


 .. figure:: ../figs/Picture8.png
    :figclass: align-center


 .. figure:: ../figs/Picture9.png
    :figclass: align-center


 Add this to the launch file inside jetbot_nav package in jetbot.launch. Here is what you can add to your launch file. The first piece of code will launch Rviz, and the second piece of code will start our node.

 .. code-block::

    <!-- Initial Pose and Goal Publisher -->
    <!-- Publish: /initialpose, /move_base_simple/goal -->
    <node pkg="rviz" type="rviz" name="rviz">
    </node>
    <!-- Subscribe: /initialpose, /move_base_simple/goal -->
    <!-- Publish: /initial_2d, /goal_2d -->
    <node pkg="localization_data_pub" type="rviz_click_to_2d" name="rviz_click_to_2d">
    </node>   

 And we have to subscribe to /initial_2d topic in the odometry node.

 Open your Odompetry.py file in catkin_ws/src/mas514/src/ and replace it by this python code

 .. literalinclude:: ../../src/Topic_Odometry_Node.py
    :language: python

Costmap configuration (global and local costmap)
--------------------------------------------------

 The ROS Navigation Stack uses two costmps to store information about obstacles in the world. 
    #.	Global costmap: This costmap is used to generate long term plans over the entire environment….for exam-ple, to calculate the shortest path from point A to point B on a map.
    #.	Local costmap: This costmap is used to generate short term plans over the environment….for example, to avoid obstacles.

 We have to configure these costmaps for our project. We set the configurations in yaml.files.

 **Common configuration (global and local costmap)**

 Let’s create a configuration file that will house parameters that are common to both the global and the local costmap. The name of this file will be **costmap_common_params.yaml**.

 Open a terminal window, and type:

    - :code:`$ roscd jetbot_nav`
    - :code:`$ mkdir param`
    - :code:`$ cd param`
    - :code:`$ gedit costmap_common_params.yaml`

 and copy these line into the yaml.file 

 .. code-block::

    obstacle_range: 0.5
    raytrace_range: 0.5
    footprint: [[-0.06, -0.08], [-0.06, 0.08],[0.14, 0.08],[0.14, -0.08]]
    #robot_radius: ir_of_robot
    inflation_radius: 0.2
    observation_sources: laser_scan_sensor 

    laser_scan_sensor: {sensor_frame: laser, data_type: LaserScan, topic: scan, marking: true, clearing: true}

 Save and close the file.

 **Global configuration (global costmap)**

 Let’s create a configuration file that will house parameters for the global costmap. The name of this file will be **global_costmap_params.yaml**.

 Open a terminal window, and type:
    
    - :code:`$ cd ~/catkin_ws`
    - :code:`$ roscd jetbot_nav`
    - :code:`$ cd param`
    - :code:`$ gedit global_costmap_params.yaml`

 and copy these line into the yaml.file

 .. code-block::

    global_costmap:
     global_frame: odom
     robot_base_frame: base_link
     update_frequency: 30.0
     static_map: true

 Save and close the file.

 **Local configuration (local costmap)**

 Let’s create a configuration file that will house parameters for the local costmap. The name of this file will be local_costmap_params.yaml.

 Open a terminal window, and type:
   
    - :code:`$ cd ~/catkin_ws`
    - :code:`$ roscd jetbot_nav`
    - :code:`$ cd param`
    - :code:`$ gedit local_costmap_params.yaml`

 and copy these line into the yaml.file

 .. code-block::

    local_costmap:
     global_frame: odom
     robot_base_frame: base_link
    update_frequency: 30.0
    publish_frequency: 30.0
    static_map: false
    rolling_window: true
    width: 1.0
    height: 1.0
    resolution: 0.1

 Save and close the file.

 **Base Local planner configuration**

 In addition to the costmap configurations we did in the previous section, we need to configure ROS Navigation Stack’s base_local_planner. The base_local_planner computes velocity commands that are sent to the robot base controller. The values that you use for your base_local_planner will depend on your robot.
    
    - :code:`$ cd ~/catkin_ws`
    - :code:`$ roscd jetbot_nav`
    - :code:`$ cd param`
    - :code:`$ gedit base_local_planner_params.yaml`
    - :code:`and copy these line into the yaml.file`

 .. code-block::

    TrajectoryPlannerROS:
      max_vel_x: 0.12
      min_vel_x: 0.11
      #max_vel_theta: 1.0
      #min_in_place_vel_theta: 0.4

      #acc_lim_theta: 3.2
      #acc_lim_x: 2.5
      #acc_lim_y: 2.5

      holonomic_robot: false
      meter_scoring: true
      xy_goal_tolerance: 0.15

 Save and close the file.

Move Base node
----------------

 Now that we have created our configuration files, we need to add them to the launch file. The configuration files will be used by ROS Navigation Stack’s  :code:`move_base node`. The move_base node is the work horse behind the scenes that is responsible for planning a collision-free path from a starting location to a goal location for a mobile robot.

 The move-base node subscribes to the following topics:
    - /move_base_simple/goal :  Goal position and orientation :code:`(geometry_msgs::PoseStamped)`. The messag-es on this topic are generated using the goal button on RViz.

 The publisher will publish to the following topics:

    - /cmd_vel : Linear and angular velocity command :code:`(geometry_msgs/Twist Message)`

 Open a terminal window, and type:
    
    - :code:`$ cd ~/catkin_ws`
    - :code:`$ roscd jetbot_nav`
    - :code:`$ cd launch`
    - :code:`$ gedit move_base.launch`

 and copy these line into the move_base.launch file.

 .. code-block:: 

    </launch>
    <!-- Map File -->
    <arg name="map_file" default="$(find jetbot_nav)/maps/map.yaml"/>
    <!-- Map Server -->
    <!-- Publish: /map, /map_metadata -->
    <node pkg="map_server" name="map_server" type="map_server" args="$(arg map_file)" />
    <!-- Add AMCL example for differential drive robots for Localization -->
    <!-- Subscribe: /scan, /tf, /initialpose, /map -->
    <!-- Publish: /amcl_pose, /particlecloud, /tf -->
    <include file="$(find amcl)/examples/amcl_diff.launch"/>
    <!-- Move Base Node -->
    <!-- Subscribe: /move_base_simple/goal -->
    <!-- Publish: /cmd_vel -->
    <node pkg="move_base" type="move_base" respawn="false" name="move_base" output="screen">
    <rosparam file="$(find jetbot_nav)/param/costmap_common_params.yaml" command="load" ns="global_costmap" />
    <rosparam file="$(find jetbot_nav)/param/costmap_common_params.yaml" command="load" ns="local_costmap" />
    <rosparam file="$(find jetbot_nav)/param/local_costmap_params.yaml" command="load" ns="local_costmap" />
    <rosparam file="$(find jetbot_nav)/param/global_costmap_params.yaml" command="load" ns="global_costmap" />
    <rosparam file="$(find jetbot_nav)/param/base_local_planner_params.yaml" command="load" />
     </node>
     </launch>

 Save and close the file.

 As you can see in the move_base.launch file, we added the map that we  created using Hector ot Gmapping SLAM, and we added ACML node for the differential drive robot.

 Now everything is done and we can launch the navigation stack!

 First we build the packages to makes sure everything is ok, open terminal and run
    
    - :code:`$ cd ~/catkin_ws/`
    - :code:`$ catkin_make --only-pkg-with-deps jetbot_nav`

 Open new terminal and launch jetbot.launch , laser.launch and move_base.launch 
   
    - :code:`$ roslaunch jetbot_nav jetbot.launch`
    - :code:`$ roslaunch jetbot_nav laser.launch`
    - :code:`$ roslaunch jetbot_nav move_base.launch`

 If necessary, set the topics for each of the RViz plugins so that you can see the axis of your robot on the screen along with the map and costmaps.

 Set the initial pose of the robot by clicking the **2D Pose Estimate** button at the top of RViz and then clicking on the map. 

 .. figure:: ../figs/Picture10.png
    :figclass: align-center

 Give the robot a goal by clicking on the 2D Nav Goal button at the top of RViz and then clicking on the map.

 .. figure:: ../figs/Picture11.png
    :figclass: align-center

 You should see the planned path automatically drawn on the map. Your robot should then begin to follow this path.

 .. figure:: ../figs/Picture12.png
    :figclass: align-center

 Open a new terminal, and see the tf tree.

    - :code:`$ rosrun tf view_frames`
    - :code:`$ evince frames.pdf`

 .. figure:: ../figs/Picture13.png
    :figclass: align-center

 Open a new terminal and see the node graph.
   
    - :code:`$ rqt_graph`

 .. figure:: ../figs/Picture14.png
    :figclass: align-center
     Node Graph

 **Thats it!**

