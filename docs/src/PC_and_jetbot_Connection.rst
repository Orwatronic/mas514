Pc and jetbot Connection 
==============================

 During the mapping and navigation we will need to control the jetbot remotely and visualize different things in Rviz without connecting the jetbot to screen and keyboard. And in order to do that we will use a pc that have ubuntu and  Ros  and use as master and the jetbot as slave. For more information visit `Robotis <https://emanual.robotis.com/docs/en/platform/turtlebot3/quick-start/#pc-setup>`_ 

 .. figure:: ../figs/Picture16.png
    :figclass: align-center

 Open new terminal in your PC and type
    
    - :code:`$ ifconfig`

 you should see the PC IP adress as follow 

 Open new terminal in your PC and type
    
    - :code:`$ ifconfig`

 .. figure:: ../figs/Picture17.png
    :figclass: align-center

 Open new terminal and type
    
    - :code:`$ nano ~/.bashrc`

 Scroll to the bottom of the file and add ths two lines

 .. code-block::

    export ROS_MASTER_URI=http://{IP_ADDRESS_OF_REMOTE_PC}:11311
    export ROS_HOSTNAME={IP_ADDRESS_OF_Remot_PC}

 then press ctrl+s to save changes and ctrl + x to exit 
    
    - :code:`$ source ~/.bashrc`

 Now, we will configure the jetbot 

 Open new terminal in your jetbot and type
    
    - :code:`$ ifconfig`

 you should see the jetbot ip idress as follow 

 .. figure:: ../figs/Picture18.png
    :figclass: align-center

 Open new terminal and type
    
    - :code:`$ nano ~/.bashrc`

 Scroll to the bottom of the file and add ths two lines

 .. code-block::

    export ROS_MASTER_URI=http://{IP_ADDRESS_OF_REMOTE_PC}:11311
    export ROS_HOSTNAME={IP_ADDRESS_OF_jetbot}

 then press ctrl+s to save changes and ctrl + x to exit 
    
    - :code:`$ source ~/.bashrc`

 Don’t forget when you just want to use the jetbot comment out the add to lines in yours bashrc file and save (you will need to restart the jetbot efter commenting out the lines)

Start connection!
------------------

Every time you use PC with the jetbot, run roscore in your pc terminal and run the nodes and launch files of the jetbot in the jetbot terminal (using visual studio), and you can run Rviz in your PC terminal and add the topics there .

Your Ready now!











